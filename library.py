from flask import Flask, jsonify, request
from flask_mysqldb import MySQL

app = Flask(__name__)
mysql = MySQL(app)

member_insert = 'insert into LIBRARY.members (name, phone, card_no, created_at, modified_at) values("%s", "%s", "%s", now(), now())'
member_update = 'insert into LIBRARY.members (name, phone, card_no, created_at, modified_at) values("%s", "%s", "%s", now(), now()) on duplicate key update name="%s", phone="%s"'
book_insert = 'insert into LIBRARY.books (title, price, pub_id, created_at, modified_at) values("%s", "%s", "%s", now(), now())'
book_update = 'insert into LIBRARY.books (title, price, pub_id, created_at, modified_at) values("%s", "%s", "%s", now(), now()) on duplicate key update title="%s", price="%s"'
book_lookup = 'select * from books where pub_id = "%s"'
member_lookup = 'select * from books where card_no = "%s"'

def create_cursor():
    conn = mysql.connection
    return conn

@app.route('/')
def users():
    data = {'Member' : {'add' : '/member/add', 'lookup' : '/member/lookup', 'update' : '/member/update', 'remove' : '/member/remove'},
	    'Book' : {'add' : '/book/add', 'lookup' : '/book/lookup', 'update' : '/book/update', 'remove' : '/book/remove'},
	    'Interact' : {'borrow' : '/interact/add', 'return' : '/interact/lookup'},}
    return jsonify(data)

@app.route('/member/add', methods=['POST'])
@app.route('/member/update', methods=['POST'])
@app.route('/member/lookup', methods=['POST'])
@app.route('/book/lookup', methods=['POST'])
@app.route('/book/update', methods=['POST'])
@app.route('/book/add', methods=['POST'])
def member_add():
    status = ''
    try:
        data = request.get_json()
    except:
	status = "Send post parameters"
    if data:
        conn = create_cursor()
        cur = conn.cursor()
	try:
	    if 'member' in request.url:
		if 'add' in request.url:
		    cur.execute(member_insert % (data['Name'], data['phone'], data['card_no']))
		elif 'update' in request.url:
		    cur.execute(member_update % (data['Name'], data['phone'], data['card_no'], data['Name'], data['phone']))
		elif 'lookup' in request.url:
		    cur.execute(member_lookup % (data['Name']))
		    
	    elif 'book' in request.url:
		if 'add' in request.url:
		    cur.execute(book_insert % (data['title'], data['price'],  data['pub_id']))
		elif 'update' in request.url:
		    cur.execute(book_update % (data['title'], data['price'],  data['pub_id'], data['title'], data['price']))
		elif 'lookup' in request.url:
		    cur.execute(member_lookup % (data['title']))
	    if 'lookup' in request.url:
		data = cur.fetchall()
		return jsonify(data)

            cur.close()
            conn.commit()
	    status = 'Success'
	except Exception as e:
	    try: 
		if 'Duplicate' in e[1]: e = "Entry Exists!! Use update endpoint to make changes"
	    except: pass
	    status = 'Insert Fail %s' % e
    return jsonify({'Status' : status})


if __name__ == '__main__':
    app.run(debug=True)
